package vlad.Pets;

import vlad.Interfaces.Foul;
import vlad.Enum.Species;

import java.util.Set;

public class DomesticCat extends Pet implements Foul {

    public DomesticCat() {
        this.species = Species.CAT;
    }

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.CAT;
    }

    @Override
    public void respond() {
        System.out.println("Mmmmrrr");
    }

    @Override
    public void foul() {
        System.out.println("meeoow!");
    }
}
