package vlad.DAO;

import vlad.Humans.Family;
import vlad.Humans.Human;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {

    private List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return index < 0 || index > families.size() - 1 ? null : families.get(index);
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index < 0 || index > families.size() - 1) {
            return false;
        } else families.remove(index);
        return true;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return families.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        for (int i = 0; i < families.size(); i++) {
            if (families.get(i).equals(family)) {
                families.set(i, family);
                return;
            }
        }
        families.add(family);
    }
}
