package vlad.Service;

import vlad.DAO.FamilyDao;
import vlad.Humans.Family;
import vlad.Humans.Human;
import vlad.Humans.Man;
import vlad.Humans.Woman;
import vlad.Pets.Pet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        System.out.println("List of all families: ");
        AtomicInteger count = new AtomicInteger();
        familyDao.getAllFamilies().forEach(family -> System.out.println(count.getAndIncrement() + 1 + ". " + family));
    }

    public List<Family> getFamiliesBiggerThan(int number) {
        System.out.println("Family bigger than " + number + " people:");
        return familyDao.getAllFamilies().stream()
                .filter(family -> number < family.countFamily())
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int number) {
        System.out.println("Family less than " + number + " people:");
        return familyDao.getAllFamilies().stream()
                .filter(family -> number > family.countFamily())
                .collect(Collectors.toList());
    }

    public int countFamiliesWithMemberNumber(int number) {
        System.out.println("Family number of people equal " + number + ":");
        return (int) familyDao.getAllFamilies().stream()
                .filter(family -> number == family.countFamily())
                .count();
    }

    public void createNewFamily(Human human1, Human human2) {
        familyDao.saveFamily(new Family(human1, human2));
    }

    public void deleteFamily(int index) {
        if (index - 1 < 0 || index - 1 > familyDao.getAllFamilies().size() - 1) {
        } else familyDao.getAllFamilies().remove(index - 1);
    }

    public Family bornChild(Family family, String nameMan, String nameWoman) {
        int random = (int) (Math.random() * 2);
        Human man = new Man(nameMan, family.getFather().getSurname());
        Human woman = new Woman(nameWoman, family.getFather().getSurname());
        if (random == 0) {
            family.addChild(man);
            familyDao.saveFamily(family);
        } else {
            family.addChild(woman);
            familyDao.saveFamily(family);
        }
        return family;
    }

    public Family adoptChild(Family family, Human human) {
        human.setFamily(family);
        human.setSurname(family.getFather().getSurname());
        family.addChild(human);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        for (Family family : familyDao.getAllFamilies()) {
            family.getChildren().removeIf(child -> child.getYourAge() != 0 && child.getYourAge() > age);
            familyDao.saveFamily(family);
        }
    }

    public int count() {
        int result = 0;
        for (Family aFamily : familyDao.getAllFamilies()) {
            result++;
        }
        return result;
    }

    public Family getFamilyById(int indexFamily) {
        System.out.println("Family by id " + indexFamily + ":");
        return familyDao.getAllFamilies().get(indexFamily - 1);
    }

    public Set<Pet> getPets(int indexFamily) {
        System.out.println("Pets family by index " + indexFamily + ":");
        return familyDao.getFamilyByIndex(indexFamily - 1).getPet();
    }

    public void addPet(int indexFamily, Pet pet) {
        Family familyByIndex = familyDao.getFamilyByIndex(indexFamily - 1);
        familyByIndex.getPet().add(pet);
        familyDao.saveFamily(familyByIndex);
    }
}
