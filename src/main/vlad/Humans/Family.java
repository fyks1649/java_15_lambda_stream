package vlad.Humans;

import vlad.Pets.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class Family {
    private Human mother;
    private Human father;
    private Set<Pet> pet;
    private List<Human> children = new ArrayList<>();

    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        mother.setFamily(this);
        father.setFamily(this);
    }

    public void addChild(Human human) {
        children.add(human);
        human.setFamily(this);
    }

    public boolean deleteChild(int index) {
        if (index > children.size() - 1 || index < 0) return false;
        children.get(index).setFamily(null);
        children.remove(index);
        return true;
    }

    public boolean deleteChild(Human child) {
        if (children.size() > 0 && children.indexOf(child) != -1) {
            children.remove(child);
            child.setFamily(null);
            return true;
        }
        return false;
    }

    public int countFamily() {
        int count = 2 + children.size();
        return count;
    }

    // overriding finalize method

    public void finalize() {
        System.out.println("Family removed!");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }

    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + children +
                ", pet=" + pet + "}";
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pet;
    }

    public void setPet(Set<Pet> pet) {
        this.pet = pet;
    }
}
